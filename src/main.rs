use clap::Parser;
use tokio::sync::Mutex;

mod metadata;
mod objects;
mod vote_storage;
mod axum_server;

use std::sync::Arc;
use vote_storage::VoteStorage;

#[derive(Parser)]
#[command(author, version, about, long_about = None, about = "next track utility")]
struct Cli {
    #[arg(short, long,  default_value = "next")]
    mode: String,

    #[arg(short, long, default_value_t = 0)]
    vote: usize,

    #[arg(long, default_value = "random_playlist.m3u")]
    playlist: String,

    #[arg(short, long, default_value_t = 8080)]
    port: usize,
}

fn main() -> Result<(), &'static str> {
    let args = Cli::parse();
    let mode = args.mode.as_ref();
    let playlist_path = &args.playlist;
    let vote_track_index = args.vote;
    let port = args.port;

    let mut vote_storage = VoteStorage::new(playlist_path, "now_voting.json", "voters.json");

    return match mode {
        "next" => vote_storage.next_track(),
        "list" => {
            let json = serde_json::to_string_pretty(&vote_storage.get_votelist())
                .map_err(|_| "Failed to serialize votelist")?;
            println!("{}", json);
            Ok(())
        }
        "vote" => {
            vote_storage.vote_by_index(vote_track_index)
        }
        "server" => {
            let vote_storage_sync = Arc::new(Mutex::new(vote_storage));
            axum_server::server_main(vote_storage_sync.clone(), port)
                .map_err(|_| "Failed to start server")
        }
        _ => {
            println!("No args");
            Ok(())
        }
    };
}
