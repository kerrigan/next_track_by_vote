use serde::{Deserialize, Serialize};
use std::path::Path;
use std::process::Command;

#[derive(Serialize, Deserialize, Debug)]
struct TrackTags {
    pub title: Option<String>,
    pub artist: Option<String>,
}

#[derive(Serialize, Deserialize, Debug)]
struct TrackFormat {
    pub tags: Option<TrackTags>,
}

#[derive(Serialize, Deserialize, Debug)]
struct TrackInfo {
    pub format: Option<TrackFormat>,
}

pub fn get_metadata(filename: String) -> String {
    let file_path = Path::new(&filename);

    let empty_info = file_path
        .file_stem()
        .and_then(|s| s.to_str())
        .unwrap_or("Unknown - Unknown")
        .to_string();

    let ffprobe_path = "/usr/bin/ffprobe";

    let output = Command::new(ffprobe_path)
        .arg("-loglevel")
        .arg("quiet")
        .arg("-print_format")
        .arg("json")
        .arg("-show_entries")
        .arg("format_tags=title,artist")
        .arg("-i")
        .arg(filename)
        .output();

    let output = match output {
        Ok(stdout) => stdout,
        Err(_err) => return empty_info.to_string(),
    };

    let stdout = output.stdout;
    let string = String::from_utf8_lossy(&stdout);
    let result: Result<TrackInfo, _> = serde_json::from_str(&string);

    if let Ok(track_info) = result {
        if let Some(format) = track_info.format {
            if let Some(tags) = format.tags {
                let mut result_info: String = "".into();
                if let Some(artist) = tags.artist {
                    result_info.push_str(&artist);
                } else {
                    return empty_info;
                    //result_info.push_str("Unknown");
                }

                result_info.push_str(" - ");

                if let Some(title) = tags.title {
                    result_info.push_str(&title);
                } else {
                    return empty_info;
                    //result_info.push_str("Unknown");
                }
                return result_info;
            } else {
                return empty_info;
            }
        } else {
            return empty_info;
        }
    } else {
        return empty_info;
    }
}
