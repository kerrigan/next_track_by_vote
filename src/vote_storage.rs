use crate::metadata;
use crate::objects::{Vote, VotePub};
use rand::prelude::SliceRandom;
use rand::thread_rng;
use std::collections::HashSet;
use std::fs;
use std::fs::{File, OpenOptions};
use std::path::Path;
use std::string::ToString;
use std::time::SystemTime;
use uuid::Uuid;

pub struct VoteStorage {
    playlist_file_path: String,
    votelist_file_path: String,
    voters_file_path: String,
    current_tracks: Vec<Vote>,
    current_votes: HashSet<String>,
}

impl VoteStorage {
    pub fn new(playlist_file_path: &str, votelist_file_path: &str, voters_file_path: &str) -> Self {
        let mut instance = VoteStorage {
            playlist_file_path: playlist_file_path.to_string(),
            votelist_file_path: votelist_file_path.to_string(),
            voters_file_path: voters_file_path.to_string(),
            current_tracks: Vec::<Vote>::new(),
            current_votes: HashSet::new(),
        };
        instance._load().unwrap();
        instance
    }

    pub fn next_track(&mut self) -> Result<(), &'static str> {
        self._clear_votes();
        let mut votelist = self.get_votelist()?;

        votelist.shuffle(&mut thread_rng());
        let item = votelist.iter().max_by_key(|x| x.votes).unwrap();

        if item.votes > 0 {
            let now = SystemTime::now()
                .duration_since(SystemTime::UNIX_EPOCH)
                .unwrap()
                .as_secs();
            let file = OpenOptions::new()
                .create(true)
                .append(true)
                .open("track_history.csv")
                .unwrap();

            let mut writer = csv::Writer::from_writer(file);
            writer
                .write_record(&[
                    item.title.clone(),
                    item.path.clone(),
                    item.votes.to_string(),
                    now.to_string(),
                ])
                .map(|_| ())
                .map_err(|_| "Failed to record next track to stats")?;
            writer
                .flush()
                .map(|_| ())
                .map_err(|_| "Failed to flush next track to csv")?;
        }
        self._new_votelist()?;
        println!("{}", &item.path.trim());
        Ok(())
    }

    pub fn get_votelist(&mut self) -> Result<Vec<Vote>, &'static str> {
        self._load()?;
        Ok(self.current_tracks.clone())
    }

    pub fn get_pub_votelist(&mut self) -> Result<Vec<VotePub>, &'static str> {
        let votelist = self.get_votelist()?;
        Ok(votelist.into_iter().map(|item| item.into()).collect())
    }

    pub fn vote_by_id(&mut self, track_id: &str, voter_id: &str) -> Result<(), ()> {
        self._load().map_err(|_| ())?;
        self._load_votes();

        if self.current_votes.contains(voter_id) {
            return Err(());
        } else {
            self.current_votes.insert(voter_id.to_string());
            self._save_votes();
        }

        self.current_tracks = self
            .current_tracks
            .iter_mut()
            .map(|item| {
                if item.id == track_id {
                    item.votes += 1;
                }
                item.clone()
            })
            .collect();
        self._save().map_err(|_| ())
        //Ok(())
    }

    pub fn vote_by_index(&mut self, track_index: usize) -> Result<(), &'static str> {
        self._load()?;
        self.current_tracks = self
            .current_tracks
            .iter_mut()
            .enumerate()
            .map(|(index, item)| {
                if index == track_index {
                    item.votes += 1;
                }
                item.clone()
            })
            .collect();
        self._save()
    }

    pub fn _clear_votes(&mut self) {
        self.current_votes.clear();
        if Path::new(&self.voters_file_path).exists() {
            fs::remove_file(&self.voters_file_path).unwrap();
        }
    }

    fn _load(&mut self) -> Result<(), &'static str> {
        let path = Path::new(&self.votelist_file_path);
        let buffer = if path.exists() {
            File::open(&self.votelist_file_path)
        } else {
            let _ = self._new_votelist();
            File::open(&self.votelist_file_path)
        }
        .unwrap();
        let votelist: Vec<Vote> =
            serde_json::from_reader(buffer).map_err(|_| "Failed to parse votelist")?;
        self.current_tracks = votelist;
        Ok(())
    }

    fn _save(&mut self) -> Result<(), &'static str> {
        let buffer = File::create(&self.votelist_file_path).unwrap();
        serde_json::to_writer_pretty(buffer, &self.current_tracks)
            .map(|_| Ok(()))
            .map_err(|_| "Failed to save current tracks")?
    }

    fn _load_votes(&mut self) {
        let path = Path::new(&self.voters_file_path);
        let buffer = if path.exists() {
            File::open(&self.voters_file_path)
        } else {
            File::create(&self.voters_file_path)
        }
        .unwrap();
        let votes: HashSet<String> = serde_json::from_reader(buffer).unwrap_or_default();
        self.current_votes = votes;
    }

    fn _save_votes(&mut self) {
        let buffer = File::create(&self.voters_file_path).unwrap();
        serde_json::to_writer_pretty(buffer, &self.current_votes).unwrap();
    }

    fn _new_votelist(&mut self) -> Result<(), &'static str> {
        let mut random = &mut thread_rng();

        let mut reader = m3u::Reader::open(&self.playlist_file_path).unwrap();
        let read_playlist: Vec<_> = reader
            .entries()
            .map(|entry| entry.unwrap())
            .filter(|item| match item {
                m3u::Entry::Path(_) => true,
                _ => false,
            })
            .map(|item| match item {
                m3u::Entry::Path(path) => String::from(path.to_str().unwrap()),
                m3u::Entry::Url(path) => path.to_string(),
            })
            .collect();

        let random_items: Vec<String> = read_playlist
            .choose_multiple(&mut random, 5)
            .cloned()
            .collect();

        let random_items: Vec<Vote> = random_items
            .iter()
            .map(|filename| Vote {
                id: Uuid::new_v4().to_string(),
                votes: 0,
                path: filename.to_string(),
                title: metadata::get_metadata(filename.to_string()),
            })
            .collect();

        self.current_tracks = random_items;
        self._save()
    }
}

#[cfg(test)]
mod test {
    use crate::vote_storage::VoteStorage;
    use std::fs;

    #[test]
    fn test_vote_by_index() {
        let mut storage =
            VoteStorage::new("random_playlist.m3u", "now_voting1.json", "voters1.json");
        storage.next_track().unwrap();
        dbg!(storage.get_votelist().unwrap());

        storage.vote_by_index(0).unwrap();
        storage.vote_by_index(0).unwrap();
        storage.vote_by_index(0).unwrap();
        storage.vote_by_index(0).unwrap();

        let current_votelist = storage.get_votelist().unwrap();

        assert_eq!(current_votelist[0].votes, 4);

        fs::remove_file("now_voting1.json").unwrap();
    }

    #[test]
    fn test_vote_by_id() {
        let mut storage =
            VoteStorage::new("random_playlist.m3u", "now_voting2.json", "voters2.json");
        storage.next_track().unwrap();
        let votelist = storage.get_votelist().unwrap();

        let user_id = "test";

        let track_id = &votelist[0].id;

        assert!(storage.vote_by_id(track_id, user_id).is_ok());
        assert!(storage.vote_by_id(track_id, user_id).is_err());

        let current_votelist = storage.get_votelist().unwrap();

        let found = current_votelist.iter().find(|&i| &i.id == track_id);
        if let None = found {
            assert!(false);
        }

        assert_eq!(found.unwrap().votes, 1);
        fs::remove_file("now_voting2.json").unwrap();
        fs::remove_file("voters2.json").unwrap();
    }
}
