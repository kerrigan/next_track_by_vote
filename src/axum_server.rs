use std::sync::Arc;

use axum::{
    extract::State,
    http::StatusCode,
    response::{Html, IntoResponse},
    routing::{get, post},
    Json, Router,
};
use tokio::sync::Mutex;
use tracing_subscriber::{
    filter::LevelFilter, prelude::__tracing_subscriber_SubscriberExt, util::SubscriberInitExt,
};

use crate::{objects::VoteInfo, vote_storage::VoteStorage};

#[tokio::main]
pub async fn server_main(
    vote_storage_sync: Arc<Mutex<VoteStorage>>,
    port: usize,
) -> Result<(), ()> {
    tracing_subscriber::registry()
        .with(tracing_subscriber::fmt::layer())
        .with(LevelFilter::WARN)
        .init();

    let app = Router::new()
        .route("/", get(index))
        .route("/vote", get(votes))
        .route("/vote", post(vote))
        .with_state(vote_storage_sync);

    axum::Server::bind(&format!("127.0.0.1:{}", port).parse().unwrap())
        .serve(app.into_make_service())
        .await
        .map_err(|_| ())
}

async fn index() -> impl IntoResponse {
    let html = include_str!("../index.html");
    Html(html)
}

async fn votes(State(vote_storage_data): State<Arc<Mutex<VoteStorage>>>) -> impl IntoResponse {
    let mut vote_storage = vote_storage_data.lock().await;
    vote_storage
        .get_pub_votelist()
        .map(|votes| Json(votes))
        .map_err(|_| (StatusCode::INTERNAL_SERVER_ERROR, "Failed to get votes"))
}

async fn vote(
    State(vote_storage_data): State<Arc<Mutex<VoteStorage>>>,
    info: Json<VoteInfo>,
) -> impl IntoResponse {
    let mut vote_storage = vote_storage_data.lock().await;
    let result = vote_storage.vote_by_id(&info.track, &info.session);
    result
        .map(|_| {
            vote_storage
                .get_pub_votelist()
                .map(|votes| Json(votes))
                .map_err(|_| (StatusCode::INTERNAL_SERVER_ERROR, "Failed to get votes"))
        })
        .map_err(|_| (StatusCode::BAD_REQUEST, "failed"))
}
