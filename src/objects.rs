use serde::{Deserialize, Serialize};

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Vote {
    pub id: String,
    pub votes: u8,
    pub path: String,
    pub title: String,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct VotePub {
    pub id: String,
    pub votes: u8,
    pub title: String,
}

impl Into<VotePub> for Vote {
    fn into(self) -> VotePub {
        return VotePub {
            id: self.id,
            votes: self.votes,
            title: self.title,
        };
    }
}

#[derive(Deserialize)]
pub struct VoteInfo {
    pub track: String,
    pub session: String,
}
